package ejercicio1;

import ejercicio1.MultipathExample;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ejercicio1Test {


    @Test
    public void C1_prueba(){ // Cumplir con a cobertura de líneas
        int datoEntradaA = 6;
        int datoEntradaB = 6;
        int c= 0;
        int salidaEsperada  =12;
        MultipathExample me = new MultipathExample();
        assertEquals(salidaEsperada , me.multiPath1(datoEntradaA, datoEntradaB , c ));

    }

    @Test
    public void C2_prueba(){ // Cumplir con la cobertura de las condiciones
        MultipathExample me = new MultipathExample();
        assertEquals(0,me.multiPath1(-1,-2,0));
    }


    @Test
    public void C3_prueba(){
        MultipathExample me = new MultipathExample();
        assertEquals(8 , me.multiPath1(3,6,2));
    }


    @MethodSource("casoE")
    @ParameterizedTest
    public void EPruebas(int a , int b , int c , int salidaEsperada ){
        MultipathExample me = new MultipathExample();
        assertEquals(salidaEsperada , me.multiPath3(a,b,c));
    }

    @MethodSource("casoD")
    @ParameterizedTest
    public void DPruebas(int a , int b , int c , int salidaEsperada){
        MultipathExample me = new MultipathExample();
        assertEquals(salidaEsperada  , me.multiPath2(a , b , c) );
    }

    public static Stream<Arguments> casoD() {
        return Stream.of(
                Arguments.of(6,3,6,15 ),
                Arguments.of(2,8, 1,1 ),
                Arguments.of(6 , 8 , 1 , 1)
                /*Arguments.of(7 , 2, 1, 1)*/


        );
    }
    public static Stream<Arguments> casoE() {
        return Stream.of(
                Arguments.of(6,3,6,15 ),
                Arguments.of(2,8, 1,1 )
                /*Arguments.of(3 , 8 , 1 , 1)
                /*Arguments.of(7 , 2, 1, 1)*/


        );
    }


}
