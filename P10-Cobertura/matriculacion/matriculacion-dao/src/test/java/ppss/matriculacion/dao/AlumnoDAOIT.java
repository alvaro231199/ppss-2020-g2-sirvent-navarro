package ppss.matriculacion.dao;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;

//import org.apache.log4j.BasicConfigurator;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.jupiter.api.*;
import ppss.matriculacion.to.AlumnoTO;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class AlumnoDAOIT {


    private IAlumnoDAO alumnoDAO; //SUT
    private FactoriaDAO factoriaDao; // Para coger al alumno
    private IDatabaseTester databaseTester;
    private IDatabaseConnection connection;

    @BeforeEach
    public void setUp() throws Exception {

        String cadena_conexionDB = "jdbc:mysql://localhost:3306/matriculacion?useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone="+ TimeZone.getDefault().getID();
        databaseTester = new JdbcDatabaseTester("com.mysql.cj.jdbc.Driver",
                cadena_conexionDB, "root", "ppss");
        //obtenemos la conexción con la BD
        connection = databaseTester.getConnection();

        //para evitar el warning al acceder a la BD
        connection.getConfig()
                .setProperty("http://www.dbunit.org/properties/datatypeFactory",
                        new MySqlDataTypeFactory());

        alumnoDAO = new FactoriaDAO().getAlumnoDAO(); // Cogemos el alumno
    }

    @Tag("Integracion-fase1")
    @Test
    public void testA1() throws Exception {
        //Vamos a poner los datos de entrada
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("33333333C");
        alumno.setNombre("Elena Aguirre Juarez");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1985);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(cal.getTime());
        //Ponemos el dataset de inicio, que será la tabla vacía
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //invocamos a nuestro SUT
        Assertions.assertDoesNotThrow(()->alumnoDAO.addAlumno(alumno));
        //recuperamos los datos de la BD después de invocar al SUT
        IDataSet databaseDataSet = connection.createDataSet();
        ITable actualTable = databaseDataSet.getTable("alumnos");
        //creamos el dataset con el resultado esperado
        IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/tabla3.xml");
        ITable expectedTable = expectedDataSet.getTable("alumnos");
        Assertion.assertEquals(expectedTable, actualTable);

    }

    @Tag("Integracion-fase1")
    @Test
    public void testA2() throws Exception {
        //Primero peraramos los datos de entrada
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("11111111A");
        alumno.setNombre("Alfonso Ramirez Ruiz");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(cal.getTime());
        //Ahora cogemos el dataset que le va a dar forma a la bd
        //Ponemos el dataset de inicio, que será la tabla vacía
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //Una vez hecho eso, llamamos al método de add y comparamos el resultado esperado
        assertThrows(DAOException.class , ()->alumnoDAO.addAlumno(alumno));
    }

    @Tag("Integracion-fase1")
    @Test
    public void testA3() throws  Exception{
        AlumnoTO alumno =  new AlumnoTO();
        alumno.setNombre(null);
        alumno.setNif("44444444D");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(cal.getTime());
        //Ahora pondremos el dataset de entrada, al igual que con las pruebas anteriores
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        //Inyectamos el dataset en el objeto databaseTester
        databaseTester.setDataSet(dataSet);
        //inicializamos la base de datos con los contenidos del dataset
        databaseTester.onSetup();
        //Comprobaremos que se lanza la excepción
        assertThrows(DAOException.class  , ()->alumnoDAO.addAlumno(alumno));

    }

    @Tag("Integracion-fase1")
    @Test
    public void testA4() throws Exception{
        AlumnoTO alumno = null;
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
        assertThrows(DAOException.class , ()->alumnoDAO.addAlumno(alumno));
    }


    @Tag("Integracion-fase1")
    @Test
    public void testA5() throws  Exception{
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNombre("Pedro Garcia Lopez");
        alumno.setNif(null);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1); //Nota: en la clase Calendar, el primer mes es 0
        cal.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(cal.getTime());
        //DE NUEVO COGEMOS EL DATASET COMO EN TODAS LAS OTRAS PRUEBAS QUE HEMOS HECHO
        IDataSet dataSet =  new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
        //Ahoira comprobamos el resulrado esperado
        assertThrows(DAOException.class , ()->alumnoDAO.addAlumno(alumno));
    }

    @Tag("Integracion-fase1")
    @Test
    public void testB1() throws Exception{
        String nifEntrada = "11111111A";
        //Ahora ponremos el dataset para configurar la bd
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
        //Ahora llamaremos a nuestro SUT
        assertDoesNotThrow(()->alumnoDAO.delAlumno(nifEntrada));
        //Ahora cogeremos el dataset de la bd y lo compararemos con el esperado
        IDataSet datasetResultado = connection.createDataSet();
        IDataSet datasetEsperado = new FlatXmlDataFileLoader().load("/tabla4.xml");
        //Ahora los compararemos para ver si son iguales
        Assertion.assertEquals(datasetEsperado.getTable("alumnos"), datasetResultado.getTable("alumnos"));

    }

    @Tag("Integracion-fase1")
    @Test
    public void testB2() throws Exception {
        String nifEntrada=  "33333333C";
        //COnfiguramos la bd para la prueba
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
        assertThrows(DAOException.class , ()->alumnoDAO.delAlumno(nifEntrada));
    }
}
