package ppss;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataArrayTest {
    private int elemento;
    private int[] coleccion;
    int contador = 0;
    private int[] resultadoReal;
    private int numElemEsperado= 0;
    private int numElemReal;

    @Test
    void addC1() {
        elemento=5;
        coleccion = new int[10];
        numElemEsperado = 1;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.add(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[0] = 5;
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }

    @Test
    void addC2(){
        elemento=4;
        contador=2;
        coleccion = new int[10];
        coleccion[0] =5;
        coleccion[1] = 2;
        numElemEsperado = 3;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.add(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[3] = 4;
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }

    @Test
    void addC3(){
        elemento=4;
        contador=10;
        coleccion = new int[10];
        for(int i = 0 ; i<10 ; i++){
            coleccion[i] = i+1;
        }
        numElemEsperado = 10;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.add(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }
    @Test
    void deleteC1(){
        elemento=5;
        contador= 0;
        coleccion = new int[10];
        numElemEsperado = 0;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.delete(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();

        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());

    }


    @Test
    void deleteC2(){
        elemento=5;
        contador= 1;
        coleccion = new int[10];
        coleccion[0] =5;
        numElemEsperado = 0;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.delete(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[0] = 0;
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }

    @Test
    void deleteC3(){
        elemento=4;
        contador= 4;
        coleccion = new int[10];
        coleccion[0] =5;
        coleccion[1] = 4;
        coleccion[2] = 3;
        coleccion[3] = 2;
        numElemEsperado = 3;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.delete(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[0] =5;
        coleccion[1] = 3;
        coleccion[2] = 2;
        coleccion[3] = 0;
        for(int i = 0 ; i< coleccion.length ; i++) {
            assertEquals(coleccion[i] , dataTest.getColeccion()[i]);
        }
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }

    @Test
    void deleteC4(){
        elemento=2;
        contador= 4;
        coleccion = new int[10];
        coleccion[0] =5;
        coleccion[1] = 4;
        coleccion[2] = 3;
        coleccion[3] = 2;
        numElemEsperado = 3;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.delete(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[0] =5;
        coleccion[1] = 4;
        coleccion[2] = 3;
        coleccion[3] = 0;
        for(int i = 0 ; i< coleccion.length ; i++) {
            assertEquals(coleccion[i] , dataTest.getColeccion()[i]);
        }
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }

    @Test
    void deleteC5(){
        elemento=1;
        contador= 4;
        coleccion = new int[10];
        coleccion[0] =5;
        coleccion[1] = 4;
        coleccion[2] = 3;
        coleccion[3] = 2;
        numElemEsperado = 4;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.delete(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[0] =5;
        coleccion[1] = 4;
        coleccion[2] = 3;
        coleccion[3] = 2;
        for(int i = 0 ; i< coleccion.length ; i++) {
            assertEquals(coleccion[i] , dataTest.getColeccion()[i]);
        }
        assertEquals(numElemReal , numElemEsperado);
        assertEquals(coleccion , dataTest.getColeccion());
    }

    @Test
    void deleteC6(){
        elemento=5;
        contador= 10;
        coleccion = new int[10];
        coleccion[0] =5;
        coleccion[1] = 4;
        coleccion[2] = 3;
        coleccion[3] = 2;
        coleccion[4] = 5;
        coleccion[5] =3;
        for(int i=6; i<10 ; i++){
            coleccion[i] =5;
        }
        numElemEsperado = 4;
        DataArray dataTest = new DataArray(coleccion , contador);
        dataTest.delete(elemento);
        resultadoReal = dataTest.getColeccion();
        numElemReal = dataTest.size();
        coleccion[0] =4;
        coleccion[1] = 3;
        coleccion[2] = 2;
        coleccion[3] = 3;
        for(int i = 4 ; i<10 ; i++) {
            coleccion[i] = 0;
        }
        for(int i = 0 ; i< coleccion.length ; i++) {
            assertEquals(coleccion[i] , dataTest.getColeccion()[i]);
        }
        assertEquals(numElemEsperado , numElemReal);
        assertEquals(coleccion , dataTest.getColeccion());
    }
}