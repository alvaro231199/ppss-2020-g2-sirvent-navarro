import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GestorLlamadasTest {




    @Test
    public void C1_GestorLlamadasTest(){
        //Creamos el mock
        IMocksControl ctrl;
        ctrl = EasyMock.createStrictControl();
        GestorLlamadas gt = EasyMock.partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock(ctrl);

        Calendario c = ctrl.createMock(Calendario.class);
        //Ponemos las expectativas
        EasyMock.expect(gt.getCalendario()).andReturn(c);
        EasyMock.expect(c.getHoraActual()).andStubReturn(10);
        ctrl.replay();
        double resultadoEsperado = 457.6d;
        assertAll(
                ()->assertEquals(resultadoEsperado , gt.calculaConsumo(22)),
                ()->ctrl.verify()
        );

    }

    @Test
    public void C2_GestorLlamadasTest(){
        //Creamos el mock
        IMocksControl ctrl;
        ctrl = EasyMock.createStrictControl();
        GestorLlamadas gt = EasyMock.partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock(ctrl);
        Calendario c = ctrl.createMock(Calendario.class);
        EasyMock.expect(gt.getCalendario()).andReturn(c);
        EasyMock.expect(c.getHoraActual()).andReturn(21);
        ctrl.replay();
        double resultadoEsperado = 136.5d;
        assertAll(
                ()->assertEquals(resultadoEsperado , gt.calculaConsumo(13)),
                ()->ctrl.verify()
        );

    }


}
