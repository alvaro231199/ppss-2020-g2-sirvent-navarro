package ppss;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;
import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.ReservaException;
import ppss.excepciones.SocioInvalidoException;

import static org.junit.jupiter.api.Assertions.*;

public class ReservaMockTest {

    @Test
    public void C1_reservaPrueba(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock(ctrl);
        //FactoriaBOs f = ctrl.createMock(FactoriaBOs.class);
       // IOperacionBO io = ctrl.createMock(IOperacionBO.class);
        String loginEntrada = "xxxx";
        String passwordEntrada="xxxx";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(false);
        ctrl.replay();
        assertAll(
                ()-> assertEquals("ERROR de permisos; " , assertThrows(ReservaException.class , ()->r.realizaReserva( loginEntrada,passwordEntrada,idEntrada , new String[] {"22222"})).getMessage()) ,
                ()->ctrl.verify()

        );

    }


    @Test
    public void C2_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock(ctrl);
        //FactoriaBOs f = ctrl.createMock(FactoriaBOs.class);

        IOperacionBO io = ctrl.createMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(idEntrada ,"2222");
        EasyMock.expectLastCall();
        io.operacionReserva(idEntrada, "33333");
        EasyMock.expectLastCall();
        //assertDoesNotThrow(()->EasyMock.expect(io.operacionReserva(idEntrada ,"2222" )));
        ctrl.replay();
        assertAll(
                ()->assertDoesNotThrow(()->r.realizaReserva( loginEntrada,passwordEntrada,idEntrada , new String[] {"2222","33333"})),
                ()->ctrl.verify()

        );

    }

    @Test
    public void C3_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock(ctrl);
        //FactoriaBOs f = ctrl.createMock(FactoriaBOs.class);
        IOperacionBO io = ctrl.createMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
       // EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(idEntrada ,"11111");
        EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());
        //assertDoesNotThrow(()->EasyMock.expect(io.operacionReserva(idEntrada ,"2222" )));
        ctrl.replay();
        assertAll(
                ()->assertEquals("ISBN invalido:11111; " , assertThrows(ReservaException.class , ()->r.realizaReserva( loginEntrada,passwordEntrada,idEntrada , new String[] {"11111"})).getMessage()),
                ()->ctrl.verify()

        );

    }


    @Test
    public void C4_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock(ctrl);
        //FactoriaBOs f = ctrl.createMock(FactoriaBOs.class);
        IOperacionBO io = ctrl.createMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Luis";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(idEntrada ,"22222");
        EasyMock.expectLastCall().andThrow(new SocioInvalidoException());
        //assertDoesNotThrow(()->EasyMock.expect(io.operacionReserva(idEntrada ,"2222" )));
        ctrl.replay();
        assertAll(
                ()->assertEquals("SOCIO invalido; " , assertThrows(ReservaException.class , ()->r.realizaReserva(  loginEntrada,passwordEntrada,idEntrada , new String[] {"22222"})).getMessage()),
                ()->ctrl.verify()

        );

    }

    @Test
    public void C5_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {
        IMocksControl ctrl = EasyMock.createStrictControl();
        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock(ctrl);
        //FactoriaBOs f = ctrl.createMock(FactoriaBOs.class);
        IOperacionBO io = ctrl.createMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(idEntrada ,"22222");
        EasyMock.expectLastCall().andThrow(new JDBCException());
        //assertDoesNotThrow(()->EasyMock.expect(io.operacionReserva(idEntrada ,"2222" )));
        ctrl.replay();
        assertAll(
                ()->assertEquals("CONEXION invalida; " , assertThrows(ReservaException.class , ()->r.realizaReserva(  loginEntrada,passwordEntrada,idEntrada , new String[] {"22222"})).getMessage()),
                ()->ctrl.verify()

        );

    }




}
