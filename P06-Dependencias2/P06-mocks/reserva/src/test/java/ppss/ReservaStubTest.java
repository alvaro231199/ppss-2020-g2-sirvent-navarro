package ppss;

import org.easymock.EasyMock;
import org.easymock.IMockBuilder;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;
import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.ReservaException;
import ppss.excepciones.SocioInvalidoException;

import static org.easymock.EasyMock.anyObject;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ReservaStubTest {


    @Test
    public void C1_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {

        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock();
        //FactoriaBOs f = EasyMock.createNiceMock(FactoriaBOs.class);
        //IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);
        String loginEntrada = "xxxx";
        String passwordEntrada="xxxx";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(false);
        EasyMock.replay(r);
        assertEquals("ERROR de permisos; " , assertThrows(ReservaException.class , ()->r.realizaReserva( loginEntrada,passwordEntrada,idEntrada , new String[] {"22222"})).getMessage());

    }


    @Test
    public void C2_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {

        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock();
        //FactoriaBOs f = EasyMock.createNiceMock(FactoriaBOs.class);
        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(EasyMock.anyString() , EasyMock.anyString());
        EasyMock.expectLastCall().asStub();
        io.operacionReserva(EasyMock.anyString() , EasyMock.anyString());
        EasyMock.expectLastCall();
        EasyMock.replay(r,io);
        assertDoesNotThrow(()->r.realizaReserva( loginEntrada,passwordEntrada,idEntrada , new String[] {"22222" , "33333"}));


    }
    @Test
    public void C3_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {

        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock();
        //FactoriaBOs f = EasyMock.createNiceMock(FactoriaBOs.class);
        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(EasyMock.anyString() , EasyMock.anyString());
        EasyMock.expectLastCall().andThrow(new IsbnInvalidoException() );
        EasyMock.replay(r,io);
        assertEquals("ISBN invalido:11111; " , assertThrows(ReservaException.class , ()->r.realizaReserva( loginEntrada,passwordEntrada,idEntrada , new String[] {"11111"})).getMessage());


    }

    @Test
    public void C4_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {

        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock();
        //FactoriaBOs f = EasyMock.createNiceMock(FactoriaBOs.class);
        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Luis";
        EasyMock.expect(r.compruebaPermisos(loginEntrada, passwordEntrada , Usuario.BIBLIOTECARIO)).andReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(EasyMock.anyString() , EasyMock.anyString());
        EasyMock.expectLastCall().andThrow(new SocioInvalidoException() );
        EasyMock.replay(r,io);
        assertEquals("SOCIO invalido; " , assertThrows(ReservaException.class , ()->r.realizaReserva(loginEntrada,passwordEntrada,idEntrada , new String[] {"22222"})).getMessage());


    }

    @Test
    public void C5_reservaPrueba() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {

        Reserva r = EasyMock.partialMockBuilder(Reserva.class).addMockedMethod("compruebaPermisos").createMock();
        //FactoriaBOs f = EasyMock.createNiceMock(FactoriaBOs.class);
        IOperacionBO io = EasyMock.createNiceMock(IOperacionBO.class);
        FactoriaBOs.setIOperacionBO(io);
        String loginEntrada = "ppss";
        String passwordEntrada="ppss";
        String idEntrada = "Pepe";
        EasyMock.expect(r.compruebaPermisos(anyObject(String.class), anyObject(String.class) ,anyObject(Usuario.class))).andStubReturn(true);
        //EasyMock.expect(f.getOperacionBO()).andReturn(io);
        io.operacionReserva(EasyMock.anyString() , EasyMock.anyString());
        EasyMock.expectLastCall().andStubThrow(new JDBCException() );
        EasyMock.replay(r, io);
        //EasyMock.replay(r,f,io);
        assertEquals("CONEXION invalida; " , assertThrows(ReservaException.class , ()->r.realizaReserva(loginEntrada,passwordEntrada,idEntrada , new String[] {"22222"})).getMessage());


    }



}
