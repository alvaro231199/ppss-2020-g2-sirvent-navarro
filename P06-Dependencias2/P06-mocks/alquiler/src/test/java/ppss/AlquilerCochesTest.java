package ppss;

import com.sun.org.apache.bcel.internal.generic.LoadClass;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.easymock.cglib.core.Local;
import org.junit.jupiter.api.Test;
import ppss.*;
import ppss.excepciones.CalendarioException;
import ppss.excepciones.MensajeException;

import java.time.LocalDate;

import static org.easymock.EasyMock.anyObject;
import static org.junit.jupiter.api.Assertions.*;


public class AlquilerCochesTest {

    @Test
    public void C1_AlquilerTest(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        AlquilerCoches ac = EasyMock.partialMockBuilder(AlquilerCoches.class).addMockedMethod("getService").createMock(ctrl);
        Servicio s = ctrl.createMock(Servicio.class);
        Calendario c = ctrl.createMock(Calendario.class);
        ac.calendario = c;
        //Ahora pondremos las expectativas
        EasyMock.expect(ac.getService()).andReturn(s);
        EasyMock.expect(s.consultaPrecio(anyObject(TipoCoche.class))).andReturn(10f);

        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(anyObject())).andReturn(false)).times(10);

        ctrl.replay();
        Ticket resultadoEsperado = new Ticket();
        resultadoEsperado.setPrecio_final(75);
        LocalDate fechaEntrada = LocalDate.of(2020,5,18);
        int diasEntrada = 10;
        assertAll(
                ()->assertEquals(resultadoEsperado.getPrecio_final() , ac.calculaPrecio(TipoCoche.TURISMO , fechaEntrada , diasEntrada  ).getPrecio_final()),
                ()->ctrl.verify()

        );

    }

    @Test
    public void C2_AlquilerTest() {
        IMocksControl ctrl = EasyMock.createStrictControl();
        AlquilerCoches ac = EasyMock.partialMockBuilder(AlquilerCoches.class).addMockedMethod("getService").createMock(ctrl);
        Servicio s = ctrl.createMock(Servicio.class);
        Calendario c = ctrl.createMock(Calendario.class);
        ac.calendario = c;
        //Ahora pondremos las expectativas
        EasyMock.expect(ac.getService()).andReturn(s);
        EasyMock.expect(s.consultaPrecio(anyObject(TipoCoche.class))).andReturn(10f);
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(anyObject())).andReturn(false).times(1).andReturn(true).times(1).andReturn(false).times(3).andReturn(true).times(1).andReturn(false).times(1));
        ctrl.replay();
        Ticket resultadoEsperado = new Ticket();
        resultadoEsperado.setPrecio_final(62.5f);
        LocalDate fechaEntrada = LocalDate.of(2020,6,19);
        int diasEntrada = 7;
        assertAll(
                ()->assertEquals(resultadoEsperado.getPrecio_final() , ac.calculaPrecio(TipoCoche.CARAVANA, fechaEntrada , diasEntrada  ).getPrecio_final()),
                ()->ctrl.verify()

        );

    }

    @Test
    public void C3_AlquilerTest(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        AlquilerCoches ac = EasyMock.partialMockBuilder(AlquilerCoches.class).addMockedMethod("getService").createMock(ctrl);
        Servicio s = ctrl.createMock(Servicio.class);
        Calendario c = ctrl.createMock(Calendario.class);
        ac.calendario = c;
        //Ahora pondremos las expectativas
        EasyMock.expect(ac.getService()).andReturn(s);
        EasyMock.expect(s.consultaPrecio(anyObject(TipoCoche.class))).andReturn(10f);
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(anyObject())).andReturn(false).times(1).andThrow(new CalendarioException()).times(1).andReturn(false).times(2).andThrow(new CalendarioException()).times(2).andReturn(false).times(2));
        ctrl.replay();
        LocalDate fechaEntrada = LocalDate.of(2020,4,17);
        int diasEntrada = 8;
        assertAll(
                ()->assertEquals( "Error en dia: 2020-04-18; " +
                                "Error en dia: 2020-04-21; " +
                                "Error en dia: 2020-04-22; " ,  assertThrows(MensajeException.class , ()->ac.calculaPrecio(TipoCoche.TURISMO, fechaEntrada , diasEntrada  ).getPrecio_final()).getMessage()),
                ()->ctrl.verify()

        );
    }


    @Test
    public void C3A_AlquilerTest(){
        IMocksControl ctrl = EasyMock.createStrictControl();
        AlquilerCoches ac = EasyMock.partialMockBuilder(AlquilerCoches.class).addMockedMethod("getService").createMock(ctrl);
        Servicio s = ctrl.createMock(Servicio.class);
        Calendario c = ctrl.createMock(Calendario.class);
        ac.calendario = c;
        //Ahora pondremos las expectativas
        EasyMock.expect(ac.getService()).andReturn(s);
        EasyMock.expect(s.consultaPrecio(anyObject(TipoCoche.class))).andReturn(10f);
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,17)))
                .andReturn(false).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,18)))
                .andThrow(new CalendarioException()).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,19)))
                .andReturn(false).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,20)))
                .andReturn(false).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,21)))
                .andThrow(new CalendarioException()).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,22)))
                .andThrow(new CalendarioException()).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,23)))
                .andReturn(false).times(1));
        assertDoesNotThrow(()->EasyMock.expect(c.es_festivo(LocalDate.of(2020,4,24)))
                .andReturn(false).times(1));
        ctrl.replay();
        LocalDate fechaEntrada = LocalDate.of(2020,4,17);
        int diasEntrada = 8;
        assertAll(
                ()->assertEquals( "Error en dia: 2020-04-18; " +
                        "Error en dia: 2020-04-21; " +
                        "Error en dia: 2020-04-22; " ,  assertThrows(MensajeException.class , ()->ac.calculaPrecio(TipoCoche.TURISMO, fechaEntrada , diasEntrada  ).getPrecio_final()).getMessage()),
                ()->ctrl.verify()

        );

    }

}
