package ppss;

import ppss.excepciones.CalendarioException;
import ppss.excepciones.MensajeException;

import java.time.LocalDate;

public class AlquilerCoches {

    protected Calendario calendario = new Calendario();

    public IService getService(){
        return new Servicio();
    }

    public Ticket calculaPrecio(TipoCoche tipo, LocalDate inicio, int ndias) throws MensajeException {
        Ticket ticket = new Ticket();
        float precioDia,precioTotal =0.0f;
        String observaciones = "";
        float porcentaje = 0.25f;
        IService servicio = getService();
        precioDia = servicio.consultaPrecio(tipo); //Haremos un mock de servicio
        for (int i=0; i<ndias;i++) {
            LocalDate otroDia = inicio.plusDays((long)i);
            try {

                if (calendario.es_festivo(otroDia)) {
                    precioTotal += (1+ porcentaje)*precioDia;
                } else {
                    precioTotal += (1- porcentaje)*precioDia;
                }
            } catch (CalendarioException ex) {
                observaciones += "Error en dia: "+otroDia+"; ";
            }
        }
        if (observaciones.length()>0) {
            throw new MensajeException(observaciones);
        }

        ticket.setPrecio_final(precioTotal);
        return ticket;
    }


}
