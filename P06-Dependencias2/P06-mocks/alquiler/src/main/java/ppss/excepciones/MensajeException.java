package ppss.excepciones;

public class MensajeException extends Exception {

    public MensajeException(String msg ){
        super(msg);
    }

}
