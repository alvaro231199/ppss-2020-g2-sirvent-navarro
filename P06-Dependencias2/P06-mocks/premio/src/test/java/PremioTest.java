import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ppss.ClienteWebService;
import ppss.Premio;
import ppss.excepciones.ClienteWebServiceException;

import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


public class PremioTest {



    @MethodSource("casosDePrueba")
    @ParameterizedTest
    public void C1_premioPruebas( float numeroEntrada , String entradaConsulta , String resultadoEsperado) {
        IMocksControl ctrl;
        ctrl = EasyMock.createStrictControl();
        Premio p = EasyMock.partialMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock(ctrl);
        ClienteWebService cl = ctrl.createMock(ClienteWebService.class);
        p.cliente = cl;
        EasyMock.expect(p.generaNumero()).andReturn(numeroEntrada);
        assertDoesNotThrow( ()->EasyMock.expect(cl.obtenerPremio()).andReturn(entradaConsulta));
        ctrl.replay();
        assertAll(
                ()->assertEquals(resultadoEsperado , p.compruebaPremio() ),
                ()->ctrl.verify()
        );

    }

    public static Stream<Arguments> casosDePrueba(){
        return Stream.of(
                Arguments.of(0.07f,"entrada final Champions" ,"Premiado con entrada final Champions" )
        );
    }
    @Test
    public void C3_premioPruebas(){
        IMocksControl ctrl;
        ctrl = EasyMock.createStrictControl();
        Premio p = EasyMock.partialMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock(ctrl);
        ClienteWebService cl = ctrl.createMock(ClienteWebService.class);
        p.cliente = cl;
        EasyMock.expect(p.generaNumero()).andReturn(0.3f);
        ctrl.replay();
        assertAll(
                ()->assertEquals("Sin premio",p.compruebaPremio()),
                ()->ctrl.verify()

        );
    }

    @Test
    public void C2_premioPruebas(){
        IMocksControl ctrl;
        ctrl = EasyMock.createStrictControl();
        Premio p = EasyMock.partialMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock(ctrl);
        ClienteWebService cl = ctrl.createMock(ClienteWebService.class);
        p.cliente = cl;
        EasyMock.expect(p.generaNumero()).andReturn(0.03f);
        assertDoesNotThrow(()->EasyMock.expect(cl.obtenerPremio()).andStubThrow(new ClienteWebServiceException("")));
        ctrl.replay();
        assertAll(

                ()->assertEquals("No se ha podido obtener el premio" , p.compruebaPremio()),
                ()->ctrl.verify()

        );

    }

}
