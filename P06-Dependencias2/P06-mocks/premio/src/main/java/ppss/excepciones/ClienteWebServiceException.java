package ppss.excepciones;

public class ClienteWebServiceException extends Exception{

    public ClienteWebServiceException(String msg){
        super(msg);
    }

}
