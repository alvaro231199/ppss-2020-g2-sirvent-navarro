package ppss;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class MatriculaTest {

     private int tasaEntrada= 500;
     private int edadEntrada= 0;
     private boolean familiaNumerosaEntrada = false;
     private boolean repetidorEntrada = false;
     private float salidaEsperada = 0;
     private float resultado= 0;

    Matricula m = new Matricula();
    @Tag("No parametrizados")
    @Test
    void C1_calculaTasaMatricula(){
        //Ponemos los datos de entrada
        edadEntrada = 10;
        familiaNumerosaEntrada= false;
        repetidorEntrada = true;
        salidaEsperada = 2000;
        resultado = m.calculaTasaMatricula(edadEntrada , familiaNumerosaEntrada , repetidorEntrada);
        assertEquals(salidaEsperada , resultado );
    }

    @Tag("No parametrizados")
    @Test
    void C2_calculaTasaMatricula() {
        edadEntrada=10;
        familiaNumerosaEntrada= false;
        repetidorEntrada = false;
        salidaEsperada= 500;
        resultado = m.calculaTasaMatricula(edadEntrada , familiaNumerosaEntrada , repetidorEntrada) ;
        assertEquals(salidaEsperada, resultado);
    }

    @Tag("No parametrizados")
    @Test
    void C3_calculaTasaMatricula() {
        edadEntrada = 80;
        familiaNumerosaEntrada = false;
        repetidorEntrada = false;
        salidaEsperada = 250;
        resultado = m.calculaTasaMatricula(edadEntrada , familiaNumerosaEntrada , repetidorEntrada) ;
        assertEquals(salidaEsperada, resultado);

    }

    @Tag("No parametrizados")
    @Test
    void C4_calculaTasaMatricula() {
        edadEntrada= 10;
        familiaNumerosaEntrada = true;
        repetidorEntrada = false;
        salidaEsperada = 250;
        resultado = m.calculaTasaMatricula(edadEntrada , familiaNumerosaEntrada , repetidorEntrada) ;
        assertEquals(salidaEsperada, resultado);
    }


    @Tag("No parametrizados")
    @Test
    void C5_calculaTasaMatricula() {
        edadEntrada = 57;
        familiaNumerosaEntrada =true;
        repetidorEntrada = false;
        salidaEsperada= 400;
        resultado = m.calculaTasaMatricula(edadEntrada , familiaNumerosaEntrada , repetidorEntrada) ;
        assertEquals(salidaEsperada, resultado);
    }
}