package ppss;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LlanosParamTest {

    private ArrayList lecturasEntrada  = new ArrayList();
    private Llanos ll = new Llanos();
    @Tag("Parametrizados")
    @BeforeEach
    public void borrar(){
        lecturasEntrada.clear();
    }
    @Tag("Parametrizados")
    @MethodSource("casosPrueba")
    @ParameterizedTest
    public  void pasaTodos(int[] lecturas , int longEsperada, int origenEsperado){
        for(int i = 0 ; i<lecturas.length ; i++){
            lecturasEntrada.add(lecturas[i]);
        }
        Tramo resultadoReal = ll.buscarTramoLlanoMasLargo(lecturasEntrada);
        assertAll(
                ()->assertEquals(longEsperada, resultadoReal.getLongitud()),
                ()->assertEquals(origenEsperado , resultadoReal.getOrigen())
        );
    }
    @Tag("Parametrizados")
    public static Stream<Arguments> casosPrueba(){

        return Stream.of(
                Arguments.of( new int[]{7,7,7} , 2, 0 ),
                Arguments.of( new int[]{7} , 0, 0 ),
                Arguments.of( new int[]{-1} , 0, 0 ),
                Arguments.of( new int[]{-1,-1,-1,-1} , 3, 0 ),
                Arguments.of( new int[] {120,140,-10 , -10, -10} , 2 ,2  )

        );


    }

}
