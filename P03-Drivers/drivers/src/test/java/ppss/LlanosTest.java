package ppss;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Objects;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.*;

public class LlanosTest {

    Llanos ll = new Llanos();
    @Tag("TablaA")
    @Tag("No parametrizados")
    @Test
    public void C1A_buscarTramoLLano(){
        //Declarar los datos de entrada
        ArrayList<Integer> lecturas = new ArrayList();
        lecturas.add(7);
        lecturas.add(7);
        lecturas.add(7);
        lecturas.add(7);
        Tramo resultadoReal = ll.buscarTramoLlanoMasLargo(lecturas);
        //Declaramos los dos resultados esperados
        int longEsperada = 3;
        int origenEsperado = 0;
        assertAll(
                ()->assertEquals(longEsperada , resultadoReal.getLongitud()),
                ()->assertEquals(origenEsperado , resultadoReal.getOrigen())
        );

    }

    @Tag("TablaA")
    @Tag("No parametrizados")
    @Test
    public void C2A_buscarTramoLLano(){
        //Declarar los datos de entrada
        ArrayList lecturas = new ArrayList();
        lecturas.add(7);
        Tramo resultadoReal = ll.buscarTramoLlanoMasLargo(lecturas);
        //Declaramos los dos resultados esperados
        int longEsperada = 0;
        int origenEsperado = 0;
        assertAll(
                ()->assertEquals(longEsperada , resultadoReal.getLongitud()),
                ()->assertEquals(origenEsperado , resultadoReal.getOrigen())
        );

    }
    @Tag("TablaB")
    @Tag("No parametrizados")
    @Test
    public void C1B_buscarTramoLLano(){
        //Declarar los datos de entrada
        ArrayList<Integer> lecturas = new ArrayList();
        lecturas.add(-1);
        Tramo resultadoReal = ll.buscarTramoLlanoMasLargo(lecturas);
        //Declaramos los dos resultados esperados
        int longEsperada = 0;
        int origenEsperado = 0;
        assertAll(
                ()->assertEquals(longEsperada , resultadoReal.getLongitud()),
                ()->assertEquals(origenEsperado , resultadoReal.getOrigen())
        );

    }
    @Tag("TablaB")
    @Tag("No parametrizados")
    @Test
    public void C2B_buscarTramoLLano(){
        //Declarar los datos de entrada
        ArrayList<Integer> lecturas = new ArrayList();
        lecturas.add(-1);
        lecturas.add(-1);
        lecturas.add(-1);
        lecturas.add(-1);
        Tramo resultadoReal = ll.buscarTramoLlanoMasLargo(lecturas);
        //Declaramos los dos resultados esperados
        int longEsperada = 3;
        int origenEsperado = 0;
        assertAll(
                ()->assertEquals(longEsperada , resultadoReal.getLongitud()),
                ()->assertEquals(origenEsperado , resultadoReal.getOrigen())
        );

    }

    @Tag("TablaB")
    @Tag("No parametrizados")
    @Test
    public void C3B_buscarTramoLLano(){
        //Declarar los datos de entrada
        ArrayList<Integer> lecturas = new ArrayList();
        lecturas.add(120);
        lecturas.add(140);
        lecturas.add(-10);
        lecturas.add(-10);
        lecturas.add(-10);
        Tramo resultadoReal = ll.buscarTramoLlanoMasLargo(lecturas);
        //Declaramos los dos resultados esperados
        int longEsperada = 2;
        int origenEsperado = 2;
        assertAll(
                ()->assertEquals(longEsperada , resultadoReal.getLongitud()),
                ()->assertEquals(origenEsperado , resultadoReal.getOrigen())
        );

    }




}
