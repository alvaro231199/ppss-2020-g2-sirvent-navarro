package ppss;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;


public class DataArrayTest {

    //Implementaremos los tests de la tabla de casos de prueba que se adjunta en el pdf

    @MethodSource("casosDePrueba")
    @ParameterizedTest
    public void C1_DataArrayPrueba(int[] elementosEntrada , int numeroElementosEntrada , int elementoABorrarEntrada, int[] salidaEsperadaColeccion , int numElementosEsperados){
        DataArray dt = new DataArray(elementosEntrada , numeroElementosEntrada);
        try {
            assertAll(
                    ()->assertArrayEquals( salidaEsperadaColeccion , dt.delete(elementoABorrarEntrada)),
                    ()->assertEquals(numElementosEsperados , dt.size()),
                    ()->assertArrayEquals(salidaEsperadaColeccion , dt.getColeccion()));
        }catch(Exception e){
            fail();
        }
    }

    @MethodSource("casosExcepciones")
    @ParameterizedTest
    public void C2_DataArrayPrueba(int[] elementosEntrada , int numeroElementosEntrada , int elementoABorrarEntrada , DataException salidaEsperada){
        DataArray dt = new DataArray(elementosEntrada , numeroElementosEntrada);

        try{
            dt.delete(elementoABorrarEntrada);
            fail();
        }catch(DataException e){
            assertEquals(salidaEsperada.getMessage() , e.getMessage());
        }

        ///OTRA FORMA DE HACERLO
        Throwable e = assertThrows(DataException.class , ()->dt.delete(elementoABorrarEntrada));
        assertEquals(salidaEsperada.getMessage() , e.getMessage());
    }


    public static Stream<Arguments> casosDePrueba(){
        return Stream.of(
                Arguments.of( new int[]{1,3,5,7,0,0,0,0,0,0},4,5,new int[]{1,3,7,0,0,0,0,0,0,0} ,3),
                Arguments.of( new int[]{1,3,3,5,7,0,0,0,0,0},5,3,new int[]{1,3,5,7,0,0,0,0,0,0} ,4),
                Arguments.of( new int[]{1,2,3,4,5,6,7,8,9,10},10,4,new int[]{1,2,3,5,6,7,8,9,10,0} ,9)
        );
    }

    public static Stream<Arguments> casosExcepciones(){
        return Stream.of(
                Arguments.of( new int[]{0,0,0,0,0,0,0,0,0,0},0,8,new DataException("No hay elementos en la colección")),
                Arguments.of( new int[]{1,3,5,7,0,0,0,0,0,0},4,-5,new DataException("El valor a borrar debe ser > cero")),
                Arguments.of( new int[]{0,0,0,0,0,0,0,0,0,0},0,0,new DataException("Colección vacía. Y el valor a borrar debe ser > cero")),
                Arguments.of( new int[]{1,3,5,7,0,0,0,0,0,0},4,8,new DataException("Elemento no encontrado"))
        );
    }

}
