package ppss;

import org.junit.jupiter.api.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class MatriculaParamTest {

    private Matricula m = new Matricula();

    @Tag("Parametrizados")
    @ParameterizedTest
    @MethodSource("casosDePrueba")
    void pruebaTodoMatriculaTest(int edadEntrada, boolean familiaNumerosaEntrada , boolean repetidorEntrada , int salidaEsperada ){ //Se repetirá para cada uno de los argumentos que se han definidio en el método de casos de prueba
            assertEquals(salidaEsperada , m.calculaTasaMatricula(edadEntrada , familiaNumerosaEntrada , repetidorEntrada)  );
    }

    @Tag("Parametrizados")
    private static Stream<Arguments > casosDePrueba(){
        return Stream.of(
                Arguments.of(65 , false, false , 250),
                Arguments.of(10, true , false, 250),
                Arguments.of(10 , false , false , 500),
                Arguments.of(10 , false , true , 2000) ,
                Arguments.of(55 , true , true , 400)

        );
    }
}
