package ppss;

public class Tramo {

    private  int longitud= 0;
    private int origen = 0;

    public int getLongitud(){
        return longitud;
    }

    public int getOrigen(){
        return origen;
    }

    public void setLongitud(int longitud){
        this.longitud = longitud;
    }

    public void setOrigen(int origen ){
        this.origen= origen;
    }
}
