package ppss;

import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;

//import org.apache.log4j.BasicConfigurator;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

/* IMPORTANTE:
    Dado que prácticamente todos los métodos de dBUnit lanzan una excepción,
    vamos a usar "throws Esception" en los métodos, para que el código quede más
    legible sin necesidad de usar un try..catch o envolver cada sentencia dbUnit 
    con un assertDoesNotThrow()
*/
public class ClienteDAO_IT {
  
  private ClienteDAO clienteDAO; //SUT
  private IDatabaseTester databaseTester;
  private IDatabaseConnection connection;

  @BeforeEach
  public void setUp() throws Exception {

    String cadena_conexionDB = "jdbc:mysql://localhost:3306/DBUNIT?useSSL=false";
    databaseTester = new JdbcDatabaseTester("com.mysql.cj.jdbc.Driver",
            cadena_conexionDB, "root", "ppss");
    //obtenemos la conexción con la BD
    connection = databaseTester.getConnection();

    //para evitar el warning al acceder a la BD
    connection.getConfig()
              .setProperty("http://www.dbunit.org/properties/datatypeFactory",
                    new MySqlDataTypeFactory());

    clienteDAO = new ClienteDAO();
  }

  @Test
  public void testInsert() throws Exception {
    Cliente cliente = new Cliente(1,"Peter", "Parker");
    cliente.setDireccion("C.UA");
    cliente.setCiudad("UA CITY");

    //Inicializamos el dataSet con los datos iniciales de la tabla cliente
    IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-init.xml");
    //Inyectamos el dataset en el objeto databaseTester
    databaseTester.setDataSet(dataSet);
    //inicializamos la base de datos con los contenidos del dataset
    databaseTester.onSetup();
     //invocamos a nuestro SUT
    Assertions.assertDoesNotThrow(()->clienteDAO.insert(cliente));

    //recuperamos los datos de la BD después de invocar al SUT
    IDataSet databaseDataSet = connection.createDataSet();
    //Recuperamos los datos de la tabla cliente
    ITable actualTable = databaseDataSet.getTable("cliente"); 

    //creamos el dataset con el resultado esperado
    IDataSet expectedDataSet = new FlatXmlDataFileLoader().load("/cliente-esperado2.xml");
    ITable expectedTable = expectedDataSet.getTable("cliente");

    Assertion.assertEquals(expectedTable, actualTable);

   }

  @Test
  public void testDelete() throws Exception {
    Cliente cliente =  new Cliente(1,"Peter", "Parker");
    cliente.setDireccion("C.UA");
    cliente.setCiudad("UA CITY");

    //inicializamos la BD
    IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-esperado2.xml");
    databaseTester.setDataSet(dataSet);
    databaseTester.onSetup();

    //invocamos a nuestro SUT
    Assertions.assertDoesNotThrow(()->clienteDAO.delete(cliente));

    IDataSet databaseDataSet = connection.createDataSet();
    int rowCount = databaseDataSet.getTable("cliente").getRowCount();
    
    assertEquals(0, rowCount);
  }

  /**
   * TESTS ADICIONALES
   *
   */
   @Test
   public void testUpdate() throws Exception {
     Cliente cliente = new Cliente(1,"John", "Smith");
     cliente.setDireccion("1 Main Street");
     cliente.setCiudad("Anycity");
     //Inicializamos el dataSet con los datos iniciales de la tabla cliente
     IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-esperado.xml");
     //Una vez que tenemos el dataset, lo metemos y hacemos un onSetUp():
     databaseTester.setDataSet(dataSet);
     databaseTester.onSetup();
     //Ahora llamamos a la función de update con los nuevos datos del cliente
     cliente.setDireccion("Other Street");
     cliente.setCiudad("NewCity");
     assertDoesNotThrow(()->clienteDAO.update(cliente));
     //Hecho eso, comparamos el dataset de la bd con el dataset que hemos creado para la prueba
     IDataSet actualBDDataset = connection.createDataSet(); // Con esto creamos un dataset de lo que hay en la bd
     IDataSet datasetEsperado = new FlatXmlDataFileLoader().load("/cliente-esperadoUp.xml"); //Con esto sacamos el dataset que es el esperado
     //AHora hacemos la comparación entre uno y otr
     Assertion.assertEquals(datasetEsperado, actualBDDataset);
     //Assertion.assertEquals(datasetEsperado.getTable("cliente"), actualBDDataset.getTable("cliente"));


   }


   @Test
   public void testRetrieve() throws Exception {
     Cliente cliente = new Cliente(1,"John", "Smith");
     cliente.setDireccion("1 Main Street");
     cliente.setCiudad("Anycity");
     //Inicializamos el dataSet con los datos iniciales de la tabla cliente
     IDataSet dataSet = new FlatXmlDataFileLoader().load("/cliente-esperado.xml");
     databaseTester.setDataSet(dataSet);
     databaseTester.onSetup();
     Cliente cl= Assertions.assertDoesNotThrow(()->clienteDAO.retrieve(cliente.getId())); //Cogemos el cliente que nos devuelve la función
     //Comparamos el cliente con el cliente que hay en la bd
     assertAll(
             ()->assertEquals(cliente.getId() , cl.getId()),
             ()->assertEquals(cliente.getApellido() , cl.getApellido()),
             ()->assertEquals(cliente.getCiudad() , cl.getCiudad()),
             ()->assertEquals(cliente.getNombre() , cl.getNombre())
     );


   }

}
