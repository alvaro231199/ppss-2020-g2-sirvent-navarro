package ppss.ejercicio2;

public class CalendarioStub extends Calendario{
    int hora=0;
    public void setHoraActual(int hora ){
        this.hora = hora;
    }

    @Override
    public int getHoraActual(){
        return this.hora;
    }
}
