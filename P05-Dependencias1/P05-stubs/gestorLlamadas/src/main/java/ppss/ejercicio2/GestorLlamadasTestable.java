package ppss.ejercicio2;

public class GestorLlamadasTestable extends GestorLlamadas {

    Calendario calendario = new Calendario();
    @Override
    public Calendario getCalendario(){
        return this.calendario;
    }

    public void setCalendario(Calendario c){
        this.calendario = c;
    }
}
