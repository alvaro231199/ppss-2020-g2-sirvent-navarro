package ppss.ejercicio2;

import java.util.Calendar;

public class GestorLlamadas {
    static double TARIFA_NOCTURNA=10.5;
    static double TARIFA_DIURNA=20.8;
    public Calendario getCalendario() {

        Calendario c = new Calendario();
        return c;
    }

    //Vamos a refactorizar parea hacer que sea testable
    public double calculaConsumo(int minutos ) {

        Calendario c = getCalendario();
        int hora = c.getHoraActual();

        if(hora < 8 || hora > 20) {

            return minutos * TARIFA_NOCTURNA;

        } else {
            return minutos * TARIFA_DIURNA;

        }
    }


}
