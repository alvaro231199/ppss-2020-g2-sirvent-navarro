package ppss.ejercicio2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GestorLlamadasTest {

    GestorLlamadasTestable g = new GestorLlamadasTestable();
    CalendarioStub c = new CalendarioStub();
    @Test
    public void C1_GestorLlamadasTest(){
        c.setHoraActual(15);
        g.setCalendario(c);
        assertEquals(208 , g.calculaConsumo(10));
    }

    @Test
    public void C2_GestorLlamadasTest(){
        c.setHoraActual(22);
        g.setCalendario(c);
        assertEquals(105,g.calculaConsumo(10));
    }

}
