package ppss.ejercicio1;

import org.junit.jupiter.api.Test;
import ppss.ejercicio1.GestorLlamadasStub;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GestorLlamadasTest {

    private GestorLlamadasStub g = new GestorLlamadasStub();


    @Test
    public void testC1(){
        int minutos = 10;
        g.setHora(15);
        assertEquals(208.0f, g.calculaConsumo(minutos) ,0.01);
    }

    @Test
    public void testC2(){
        int minutos = 10;
        g.setHora(22);
        assertEquals(105, g.calculaConsumo(minutos));
    }


}
