package ppss;

import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.SocioInvalidoException;

public class OperacionStub implements IOperacionBO {

    Throwable exp;
    public void setResultado(Throwable t ){
        exp = t;
    }

    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
        if(exp!=null){
            if (exp.getMessage().contains("Isbn")) throw new IsbnInvalidoException();
            if(exp.getMessage().contains("Socio")) throw new SocioInvalidoException();
            if(exp.getMessage().contains("Conexion")) throw new JDBCException();
        }
    }

}
