import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ppss.Operacion;
import ppss.OperacionStub;
import ppss.ReservaTestable;
import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.ReservaException;
import ppss.excepciones.SocioInvalidoException;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ReservaTest {

    ReservaTestable rt = new ReservaTestable();
    OperacionStub op = new OperacionStub();

    @MethodSource("casosPrueba")
    @ParameterizedTest
    public void C1_reservaPrueba(String loginEntrada , String passwordEntrada , String idSocioEntrada, String[] isbnsEntrada ,Throwable t , String salidaEsperada){
        op.setResultado(t);
        rt.setIOperacionBO(op);

        Throwable exp = assertThrows(ReservaException.class ,
                ()->rt.realizaReserva(loginEntrada,passwordEntrada,idSocioEntrada , isbnsEntrada));
        assertEquals(salidaEsperada , exp.getMessage());

    }

    @Test
    public void C2_reservaPrueba() {
        String loginEntrada="ppss";
        String passwordEntrada= "ppss";
        String idSocioEntrada= "Luis";
        String[] isbnsEntrada = new String[] {"11111" , "22222"};
        op.setResultado(null);
        rt.setIOperacionBO(op);
        assertDoesNotThrow(()->rt.realizaReserva(loginEntrada,passwordEntrada,idSocioEntrada , isbnsEntrada));


    }


    public  static Stream<Arguments> casosPrueba(){
        return Stream.of(
            Arguments.of("xxxx","xxxx","Luis" , new String[] {"11111"} , null , "ERROR de permisos; "),
            Arguments.of("ppss","ppss","Luis" , new String[] {"33333"} , new Throwable("Isbn"), "ISBN invalido:33333; "),
            Arguments.of("ppss","ppss","Luis" , new String[] {"11111"} , new Throwable("Socio"), "SOCIO invalido; "),
            Arguments.of("ppss","ppss","Luis" , new String[] {"11111"} , new Throwable("Conexion"), "CONEXION invalida; ")
                );

    }





}
