

import java.time.LocalDate;

public class AlquilerCoches {

    //Implrementaremos el método para que sea refactorizable, dado que lo vamos refactorizar por factoría local
    public IService getService(){
        IService servicio = new Servicio();
        return servicio;
    }

    protected Calendario calendario = new Calendario();

    public Ticket calculaPrecio(TipoCoche tipo, LocalDate inicio, int ndias) throws MensajeException {

        Ticket ticket = new Ticket();
        float precioDia,precioTotal =0.0f;
        float porcentaje = 0.25f;

        IService servicio = getService(); //Necesitamos refactorizar esta parte

        precioDia = servicio.consultaPrecio(tipo);

        String observaciones = "";
        for (int i = 0; i<ndias; i++) {

            LocalDate otroDia = inicio.plusDays((long)i);

            try {
                if (calendario.es_festivo(otroDia)) {
                    precioTotal += (1+ porcentaje)*precioDia;
                } else {
                    precioTotal += (1- porcentaje)*precioDia;

                }

            } catch (CalendarioException ex) {

                observaciones += "Error en dia: "+otroDia+"; ";

            }

        }

        if (observaciones.length()>0) {

            throw new MensajeException(observaciones);

        }

        ticket.setPrecio_final(precioTotal);

        return ticket;
    }


}
