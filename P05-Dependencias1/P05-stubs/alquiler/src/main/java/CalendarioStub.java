import java.time.LocalDate;

public class CalendarioStub extends Calendario {

    LocalDate[] diasFestivos = {};
    LocalDate[] diasException = {};
    public void setDiasFestivos(LocalDate[] dias){
        this.diasFestivos = dias;
    }
    public void setDiasException(LocalDate[] dias){
        this.diasException = dias;
    }

    @Override
    public boolean es_festivo(LocalDate otroDia) throws CalendarioException {

        for(int i= 0 ; i<diasFestivos.length ; i++){
            if(diasFestivos[i].equals(otroDia)){

                return true;
            }
        }
        for(int i = 0; i<diasException.length ; i++){
            if(diasException[i].equals(otroDia)){
                throw new CalendarioException("Error en dia: " + otroDia);
            }
        }

        return false;
    }

}
