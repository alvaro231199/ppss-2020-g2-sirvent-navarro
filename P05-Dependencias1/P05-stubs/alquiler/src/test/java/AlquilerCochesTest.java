

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class AlquilerCochesTest {

    CalendarioStub c = new CalendarioStub();
    AlquilerCochesTestable ac = new AlquilerCochesTestable();
    ServicioStub s = new ServicioStub();

    @Test
    public void C1_AlquilerCochePruebas() {
        LocalDate inicioEntrada = LocalDate.of(2020, 05, 18);
        int diasEntrada = 10;
        Ticket t = new Ticket();
        t.setPrecio_final(75);
        ac.calendario = c;
        ac.setService(s);
        try {
            Ticket resultado = ac.calculaPrecio(TipoCoche.TURISMO, inicioEntrada, diasEntrada);
            assertEquals(t.getPrecio_final(), resultado.getPrecio_final());
        } catch (MensajeException e) {
            fail();
        }
    }

    @Test
    public void C2_AlquilerCochePruebas(){
        LocalDate inicioEntrada = LocalDate.of(2020,06,19);
        int diasEntrada = 7;
        Ticket t = new Ticket();
        t.setPrecio_final(62.5f);
        ac.setService(s);
        c.setDiasFestivos(new LocalDate[] {LocalDate.of(2020,06,20) , LocalDate.of(2020,06,24)});
        ac.calendario = c;
        try {
            Ticket resultado = ac.calculaPrecio(TipoCoche.CARAVANA, inicioEntrada, diasEntrada);
            assertEquals(t.getPrecio_final() , resultado.getPrecio_final());
        }catch (MensajeException e ){
            fail();
        }
    }

    @Test
    public void C3_AlquilerCochePruebas(){
        LocalDate inicioEntrada = LocalDate.of(2020,04,17);
        int diasEntrada = 8;
        ac.setService(s);
        c.setDiasException(new LocalDate[] {LocalDate.of(2020,04,18) , LocalDate.of(2020,04,21) , LocalDate.of(2020,04,22)});
        ac.calendario = c;
        String resultEsperado = "Error en dia: 2020-04-18; " +
                "Error en dia: 2020-04-21; " +
                "Error en dia: 2020-04-22; ";
        try {
            Ticket resultado = ac.calculaPrecio(TipoCoche.TURISMO, inicioEntrada, diasEntrada);
            fail();
        }catch (MensajeException e ){
            assertEquals(resultEsperado , e.msg);
        }
    }
}