package ejercicio2.conPO;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class LoginPage {
    WebDriver driver;
    WebElement usu;
    WebElement password;
    WebElement log;
    public LoginPage(WebDriver driver){
        this.driver = driver;
        this.driver.get("http://demo.guru99.com/V5");
        this.driver.manage().timeouts().implicitlyWait(10 , TimeUnit.SECONDS );
        this.usu = driver.findElement(By.name("uid"));
        this.password = driver.findElement(By.name("password"));
        this.log = driver.findElement(By.name("btnLogin"));
    }

    public void login(String usuTest,  String passwordTest){
        this.usu.sendKeys(usuTest);
        this.password.sendKeys(passwordTest);
        this.log.click();

    }
    public String loginIncorrect(String usuTest , String passwordTest ){
        this.usu.sendKeys(usuTest);
        this.password.sendKeys(passwordTest);
        this.log.click();
        String res = this.driver.switchTo().alert().getText();
        this.driver.switchTo().alert().accept();
        return res;
    }

    public String getpageTitle(){
        return this.driver.getTitle();
    }


}
