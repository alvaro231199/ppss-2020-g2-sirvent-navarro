package ejercicio2.conPO;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManagerPage {

    WebDriver driver;
    WebElement homePage;
    WebElement logOut;

    public ManagerPage(WebDriver driver) {
        this.driver = driver;
        this.logOut = driver.findElement(By.linkText("Log out"));
        this.homePage = driver.findElement(By.xpath("//marquee[@class='heading3']"));

    }

    public String getHomePageDashBoardUserName(){
        return homePage.getText();
    }
}