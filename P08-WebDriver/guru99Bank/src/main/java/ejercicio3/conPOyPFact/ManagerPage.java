package ejercicio3.conPOyPFact;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManagerPage {
    WebDriver driver;
    @FindBy(xpath="//marquee[@class='heading3']") WebElement homePageText;
    @FindBy(linkText = "New Customer") WebElement newCustomer;
    @FindBy(linkText = "Delete Customer") WebElement deleteCustomer;
    @FindBy(linkText="Log out") WebElement logout;


    public ManagerPage(WebDriver driver ){
        this.driver = driver;
    }

    public String getText(){
        return this.homePageText.getText();
    }

    public NewCustomerPage newCustomerPage(){
        this.newCustomer.click();
        return  PageFactory.initElements(driver, NewCustomerPage.class);
    }

    public DeleteCustomerPage deleteCustomerPage(){
        this.deleteCustomer.click();
        return  PageFactory.initElements(driver, DeleteCustomerPage.class);
    }


}
