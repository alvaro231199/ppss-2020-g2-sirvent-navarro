package ejercicio3.conPOyPFact;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NewCustomerPage {
    WebDriver driver;
    @FindBy(name="name") WebElement name;
    @FindBy(xpath = "//input[@value='m']") WebElement male;
    @FindBy(xpath="//input[@value='f']") WebElement female;
    @FindBy(name="dob") WebElement date;
    @FindBy(name="addr") WebElement addr;
    @FindBy(name="city") WebElement city;
    @FindBy(name="state") WebElement state;
    @FindBy(name="pinno") WebElement pin;
    @FindBy(name="telephoneno") WebElement tp;
    @FindBy(name="emailid") WebElement emailID;
    @FindBy(name="password") WebElement passwd;
    @FindBy(xpath="//input[@name='sub']") WebElement submit;
    @FindBy(xpath = "//p[@class='heading3']") WebElement heading;
    @FindBy(xpath = "//a[@href='Managerhomepage.php']") WebElement managerPage;
    public NewCustomerPage(WebDriver driver){
        this.driver = driver;
    }

    public void fillForm(String name , boolean sex , String date , String addr , String city  , String state , String pin , String tp , String emailID , String passwd){
        this.name.sendKeys(name);
        if(sex == true ){
            this.male.click();
        }else this.female.click();
        this.state.sendKeys(state);
        this.date.sendKeys(date);
        this.addr.sendKeys(addr);
        this.city.sendKeys(city);
        this.pin.sendKeys(pin);
        this.emailID.sendKeys(emailID);
        this.tp.sendKeys(tp);
        this.passwd.sendKeys(passwd);
    }

    public RegisteredPage submit(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //This will scroll the page till the element is found
        js.executeScript("arguments[0].scrollIntoView();", submit);
        //ahora ya está visible el botón en la página y ya podemos hacer click sobre él
        submit.click();
        //this.submit.click();
        return  PageFactory.initElements(driver, RegisteredPage.class);
    }
    public void submitError(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //This will scroll the page till the element is found
        js.executeScript("arguments[0].scrollIntoView();", submit);
        //ahora ya está visible el botón en la página y ya podemos hacer click sobre él
        submit.click();
        this.driver.switchTo().alert().accept();
    }
    public String getHeading (){
        return this.heading.getText();
    }
    public ManagerPage returnToManagerPage(){
        this.managerPage.click();
        return PageFactory.initElements(driver, ManagerPage.class);
    }

}
