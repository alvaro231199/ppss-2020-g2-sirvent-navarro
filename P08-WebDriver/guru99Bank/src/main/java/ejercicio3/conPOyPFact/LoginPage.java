package ejercicio3.conPOyPFact;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LoginPage {
    WebDriver driver;
    @FindBy(name="uid") WebElement user;
    @FindBy(name="password") WebElement password;
    @FindBy(name="btnLogin") WebElement login;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        this.driver.get("http://demo.guru99.com/V5");
        this.driver.manage().timeouts().implicitlyWait(10 , TimeUnit.SECONDS );

    }

    public ManagerPage login(String user, String passwd){
        this.user.sendKeys(user);
        this.password.sendKeys(passwd);
        this.login.click();
        return  PageFactory.initElements(driver, ManagerPage.class);
    }

    public String getLoginTitle(){
        return this.driver.getTitle();
    }

}
