package ejercicio3.conPOyPFact;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisteredPage {
    WebDriver driver;
    @FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[4]/td[2]") WebElement usuID;
    @FindBy(linkText = "Continue") WebElement cont;
    @FindBy(xpath="//p[@class='heading3']") WebElement cabecera;
    public RegisteredPage(WebDriver driver){
        this.driver = driver;
    }

    public String getUsuID(){
        System.out.println("Value " + usuID.getText());
        return this.usuID.getText();
    }

    public ManagerPage continueClick(){
        this.cont.click();
        return PageFactory.initElements(driver, ManagerPage.class);
    }

    public String getText(){
        return this.cabecera.getText();
    }
}
