package ejercicio3.conPOyPFact;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DeleteCustomerPage {
    WebDriver driver;
    @FindBy(name="cusid") WebElement usuID;
    @FindBy(name="AccSubmit") WebElement sub;
    public DeleteCustomerPage(WebDriver driver){
        this.driver = driver;
    }

    public ManagerPage deleteCustomer(String id ){
        this.usuID.sendKeys(id);
        this.sub.click();
        this.driver.switchTo().alert().accept();
        //this.driver.switchTo().alert().accept();
        return  PageFactory.initElements(driver, ManagerPage.class);
    }



}
