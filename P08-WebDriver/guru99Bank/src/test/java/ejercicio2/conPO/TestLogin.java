package ejercicio2.conPO;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.jupiter.api.Assertions.fail;

public class TestLogin {


    @Test
    public void test_Login_Correct(){
        WebDriver driver = new FirefoxDriver();
        LoginPage log = new LoginPage(driver);
        Assertions.assertTrue(log.getpageTitle().toLowerCase().contains("guru99 bank"));
        log.login("mngr260820", "YvegAme");
        ManagerPage mp = new ManagerPage(driver);
        Assertions.assertTrue(mp.getHomePageDashBoardUserName().toLowerCase().contains("welcome"));
        driver.close();
    }

    @Test
    public void test_Login_Incorrect(){
        WebDriver driver = new FirefoxDriver();
        LoginPage log = new LoginPage(driver);
        Assertions.assertTrue(log.getpageTitle().toLowerCase().contains("guru99 bank"));
        Assertions.assertEquals("User is not valid" , log.loginIncorrect("usuNoVal", "1234"));

            //ManagerPage mp = new ManagerPage(driver);
        //Assertions.assertFalse(mp.getHomePageDashBoardUserName().toLowerCase().contains("welcome"));
        driver.close();

    }
}
