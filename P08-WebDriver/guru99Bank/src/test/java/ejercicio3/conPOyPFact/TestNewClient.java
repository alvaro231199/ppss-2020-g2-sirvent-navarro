package ejercicio3.conPOyPFact;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

public class TestNewClient {

    @Test
    public void testTestNewCLienteOk(){
        WebDriver driver = new FirefoxDriver();
        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);
        Assertions.assertTrue( lp.getLoginTitle().toLowerCase().contains("guru99 bank"));
        ManagerPage mp = lp.login("mngr260820", "YvegAme");
        Assertions.assertTrue(mp.getText().toLowerCase().contains("welcome"));
        NewCustomerPage nc = mp.newCustomerPage();
        Assertions.assertTrue(nc.getHeading().toLowerCase().contains("add new customer"));
        nc.fillForm("sn" , false , "1999-11-23" , "Calle X" , "Alicante" , "Alicante" , "123456", "999999999" , "sn25@alu.ua.es", "123456" );
        RegisteredPage rp = nc.submit();
        String usuID = rp.getUsuID();
        Assertions.assertTrue(rp.getText().toLowerCase().contains("successfully"));
        mp = rp.continueClick();
        DeleteCustomerPage dp = mp.deleteCustomerPage();
        dp.deleteCustomer(usuID);

        driver.close();
    }

    @Test
    public  void testTestNewClientDuplicate(){
        WebDriver driver = new FirefoxDriver();
        LoginPage lp = PageFactory.initElements(driver, LoginPage.class);
        Assertions.assertTrue(lp.getLoginTitle().toLowerCase().contains("guru99 bank"));
        ManagerPage mp = lp.login("mngr260820","YvegAme");
        Assertions.assertTrue(mp.getText().toLowerCase().contains("welcome"));
        NewCustomerPage nc = mp.newCustomerPage();
        Assertions.assertTrue(nc.getHeading().toLowerCase().contains("add new customer"));
        nc.fillForm("sn" , true , "1999-11-23" , "Calle X" , "Alicante" , "Alicante" , "123456", "999999999" , "sn25@alu.ua.es", "123456" );
        RegisteredPage rp = nc.submit();
        String usuID = rp.getUsuID();
        Assertions.assertTrue(rp.getText().toLowerCase().contains("successfully"));
        mp = rp.continueClick();
        nc = mp.newCustomerPage();
        Assertions.assertTrue(nc.getHeading().toLowerCase().contains("add new customer"));
        nc.fillForm("sn" , false , "1999-11-23" , "Calle X" , "Alicante" , "Alicante" , "123456", "999999999" , "sn25@alu.ua.es", "123456" );
        nc.submitError();
        mp = nc.returnToManagerPage();
        Assertions.assertTrue(mp.getText().toLowerCase().contains("welcome"));
        DeleteCustomerPage dc = mp.deleteCustomerPage();
        dc.deleteCustomer(usuID);
        driver.close();


    }
}
