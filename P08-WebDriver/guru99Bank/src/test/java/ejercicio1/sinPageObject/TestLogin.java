package ejercicio1.sinPageObject;

import org.junit.jupiter.api.Test;
import org.omg.CORBA.TIMEOUT;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.text.html.HTMLEditorKit;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.fail;

public class TestLogin {

    @Test
    public void test_Login_Correct() {
        WebDriver driver = new FirefoxDriver();

        driver.get("http://demo.guru99.com/V5"); //Cogemos la url
        driver.manage().timeouts().implicitlyWait(10 , TimeUnit.SECONDS);
        WebElement usu = driver.findElement(By.name("uid"));
        WebElement pswd = driver.findElement(By.name("password"));
        WebElement log = driver.findElement(By.name("btnLogin"));
        Actions builder = new Actions(driver);
        Action compositeAction = builder.sendKeys(usu,"mngr260820")
                .sendKeys(pswd , "YvegAme").click(log)
                .build();
        compositeAction.perform();
        driver.close();

    }
    @Test
    public void test_Login_Incorrect(){
        WebDriver driver = new FirefoxDriver();
        driver.get("http://demo.guru99.com/V5");
        driver.manage().timeouts().implicitlyWait(10 , TimeUnit.SECONDS);
        WebElement usu = driver.findElement(By.name("uid"));
        WebElement pswd = driver.findElement(By.name("password"));
        WebElement log = driver.findElement(By.name("btnLogin"));
        usu.sendKeys("usuFalso");
        pswd.sendKeys("123");
        log.click();
        driver.switchTo().alert().accept();
        driver.close();
    }




























}
